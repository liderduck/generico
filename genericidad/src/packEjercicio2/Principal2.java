package packEjercicio2;

import java.util.ArrayList;
import java.util.List;

public class Principal2 {
	public static List<Persona> crearListaPersonas() {
        
        List<Persona> lista = new ArrayList<Persona>();
        lista.add(
                new Persona("Ane", "12345678-F", 12));
        lista.add(
	            new Persona("Mikel", "98745678-G", 3));
        lista.add(
	            new Persona("Joseba", "12347895-I", 72));
        lista.add(
	            new Persona("Miren", "98764123-Z", 33));
        lista.add(
	            new Persona("Iker", "78965678-F", 12));
        
        return lista;
    }
 
 public static void main(String... args) {
        
        // Crear la lista para las pruebas
        List<Persona> z = crearListaPersonas();
    
        // 1.- Mostrar la list
        System.out.println("Lista:");
        z.stream().forEach(System.out::println);
        System.out.println();
        
        // 2.- Adinen batazbestekoak kalkulatu
        System.out.println("Edad media:");
        double bb = z.stream().mapToInt(p->p.getEdad()).average().getAsDouble();
        System.out.println(bb);
        System.out.println();
        
        // 3.- Batazbestekoa baina zaharragoak direnak pantailaratu
        System.out.println("Mayores que la media:");
        z.stream().filter(p -> (double)p.getEdad() > bb).forEach(System.out::println);
        System.out.println();
        
    
 }


}
