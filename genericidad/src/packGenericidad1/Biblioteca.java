package packGenericidad1;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

		private String nombre;
		private List<Estanteria> estanterias;
		

		public Biblioteca (String pNombre){
			this.nombre = pNombre;
			estanterias = new ArrayList<Estanteria>();
		}
		
		public String getNombre() {
			return this.nombre;
		}
		
		public void addEstanterias(Estanteria pEstanteria){
			estanterias.add(pEstanteria);
		}
		
		public void mostrarLibrosEstanterías(){
			estanterias.stream().forEach(a->a.ordenarPorTitulo());
			estanterias.stream().sorted((a1,a2) -> a1.getArea().compareTo(a2.getArea())).forEach(System.out::println);
		}
		
		public void mostrarMayor(int pLimite){
			estanterias.stream().forEach(a->a.mostrarMayor(pLimite));
		}
		
		public void mostrarTodosLibros(){
			estanterias.stream().forEach(a->a.mostrarTodos());
		}
		
		public void mostrarTodosOrdenados(){
			estanterias.stream().forEach(a->a.mostrarOrdenados());
		}
		
		public void mostrarPrecioMedio(){
			estanterias.stream().forEach(a->a.mostrarPrecioMedio());
		}
		
		public void mostrarLugar(){
			estanterias.stream().map(Estanteria::getLugar).forEach(System.out::println);
		}
		
		public void mostrarArea(){
			estanterias.stream().map(Estanteria::getArea).forEach(System.out::println);
		}
		
		public void mostrarAreaOrdenada(){
			estanterias.stream().sorted((a1,a2)->a1.getArea().compareTo(a2.getArea())).map(Estanteria::getArea).forEach(System.out::println);
		}
		
		public void devolverLibrosCaros(int n){
			estanterias.stream().forEach(a->a.librosCaros(n));
		}
		
		
		
		public static Biblioteca crearBiblioteca() {
			Biblioteca l = new Biblioteca ("EUITI");
			
			Estanteria terror = new Estanteria ("6A7B", "Terror");
			terror.addLibro(new Libro("Drakula", "Bram Stoker", 22));
			terror.addLibro(new Libro("Frankestein", "Mary Shelley", 36));
			terror.addLibro(new Libro("Cuentos", "Edgar Allan Poe", 27));
			l.addEstanterias(terror);
			
			Estanteria cienciaFiccion = new Estanteria ("3X78", "Ciencia Ficcion");
			cienciaFiccion.addLibro(new Libro("The Martian", "Andy Weir", 27));
			cienciaFiccion.addLibro(new Libro("Dune", "Frank Herbert", 16));
			cienciaFiccion.addLibro(new Libro("Hyperion", "Dan Simmons", 9));
			l.addEstanterias(cienciaFiccion);
			
			Estanteria aventura = new Estanteria("43X8", "Aventura");
			aventura.addLibro(new Libro("La Isla del Tesoro", "Robert Louis Stevenson", 22));
			aventura.addLibro(new Libro("Tom Sawyer-en Abenturak", "Mark Twain", 23));
			aventura.addLibro(new Libro("Las minas del rey Salomon", "Henry Rigger Haggard", 9));
			l.addEstanterias(aventura);
			
			return l;
		}
		
		public static void main(String... args) {
	        
	        // Probetarako liburutegia sortu
	        Biblioteca bibliotecaEuiti = crearBiblioteca();
	    
	        // 1.- Mostrar estanterías de la Biblioteca
	        System.out.println("Biblioteca: "+ bibliotecaEuiti.getNombre());
	        bibliotecaEuiti.mostrarLibrosEstanterías();
	        System.out.println();
	        
	        // 2.- Libros que cuesten mas de 18 euros ordenados por autor.
	        System.out.println("Libros que cuestan más de 18 euros, ordenados por autor: ");
	        bibliotecaEuiti.mostrarMayor(18);
	        
	        // 3- mostrar todos los libros de todas las estanterias
	        bibliotecaEuiti.mostrarTodosLibros();
	        System.out.println();
	        
	        // 4- mostrar todos los libros de todas las estanterias ordenados por autor.
	        bibliotecaEuiti.mostrarTodosOrdenados();
	        System.out.println();
	        
	        // 5- mostrar el precio medio de todos los libros por estanteria
	        bibliotecaEuiti.mostrarPrecioMedio();
	        System.out.println();
	        
	        //6- mostrar el lugar de cada estanteria
	        bibliotecaEuiti.mostrarLugar();
	        System.out.println();
	        
	        //7-mostrar el area de cada estanteria
	        bibliotecaEuiti.mostrarArea();
	        System.out.println();
	        
	        //8-mostrar el area de cada estanteria ordenadas
	        bibliotecaEuiti.mostrarAreaOrdenada();
	        System.out.println();
	        
	        //9-devolver los N libros mas caros por cada estanteria
	        int n=3;
	        bibliotecaEuiti.devolverLibrosCaros(n);
	        
	        
	 }

}
