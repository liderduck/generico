package packGenericidad1;

import java.util.ArrayList;
import java.util.List;

public class Estanteria {
	private String lugar;
	private String area;
	private List<Libro> libros;
	
	public Estanteria(String pLugar, String pArea){
		this.lugar = pLugar;
		this.area = pArea;
		libros = new ArrayList<Libro>();
	}
	
	public String getLugar(){
		return this.lugar;
	}
	
	public String getArea() {
		return this.area;
	}
	
	public String toString() {
		  String imprimirLib = "";
		  for (Libro l:libros){
			  imprimirLib += "\n\t" + l.toString();
		  }
	      return String.format("\tLugar:%1$s\tArea: %2$s"+ imprimirLib, getLugar(), getArea());
	}
	
	public void addLibro (Libro pLibro){
		libros.add(pLibro);
	}
	
	public void ordenarPorTitulo(){
		libros.sort((l1,l2)->l1.getTitulo().compareTo(l2.getTitulo()));
	}
	
	public void mostrarMayor (int pPrecio){
		libros.stream().filter(l->l.getPrecio()>=pPrecio).sorted((l1,l2)->l1.getAutor().compareTo(l2.getAutor())).forEach(System.out::println);
	}
	
	public void mostrarTodos(){
		libros.stream().forEach(System.out::println);
	}
	
	public void mostrarOrdenados(){
		libros.stream().sorted((l1,l2)->l1.getAutor().compareTo(l2.getAutor())).forEach(System.out::println);
	}
	
	public void mostrarPrecioMedio(){
		double zz=libros.stream().mapToInt(Libro::getPrecio).average().getAsDouble();
		System.out.println(zz);
	}
	
	public void librosCaros(int n){
		libros.stream().sorted((l1,l2)-> Integer.compare(l2.getPrecio(), l1.getPrecio())).limit(1).forEach(System.out::println);
		
	}

}
