package packEjercicio3;

import java.util.ArrayList;
import java.util.List;

public class Principal3 {
	public static List<Libro> crearListaLibros() {
		
		List<Libro> lz = new ArrayList<Libro>();
		
		lz.add(new Libro("Drakula", "Bram Stoker", 220));
		lz.add(new Libro("Frankestein", "Mary Shelley", 360));
		lz.add(new Libro("Cuentos", "Edgar Allan Poe", 2700));
		lz.add(new Libro("The Martian", "Andy Weir", 270));
		lz.add(new Libro("Dune", "Frank Herbert", 1600));
		lz.add(new Libro("Hyperion", "Dan Simmons", 900));
		lz.add(new Libro("La isla del tesoro", "Robert Louis Stevenson", 2200));
		lz.add(new Libro("Las aventuras de Tom Sawyer", "Mark Twain", 230));
		lz.add(new Libro("Las minas del rey Salomon", "Henry Rigger Haggard", 909));
		
		return lz;
	}
	
	public static void main(String... args) {
		
        // Crar la lista de libros para la Prueba
        List<Libro> l= Principal3.crearListaLibros();
    
        // 1.- Mostar la lista de libros
        System.out.println("Libros:");
        l.stream().forEach(System.out::println);
        System.out.println();
        
        // 2.- Mostrar los libros que tienen m�s de 1000 paginas
        System.out.println("Libros que tienen m�s de 1000 paginas: ");
        l.stream().filter(lib->lib.getNumPaginas()>1000).forEach(System.out::println);;
 }
}
